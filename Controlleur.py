
# -*- coding: UTF-8 -*-

import Modele
import Vue

################################################
# Classe Controleur
################################################

class Controleur():
    def __init__(self):
        self.modele=Modele.Jeu(self)
        self.vue=Vue.Vue(self)
        self.vue.afficheJeu(self.modele)
        
    def prochaincoup(self,coup):
        self.modele.prochaincoup(coup)
        
        
if __name__ == '__main__':
    c=Controleur()
    
    