
# -*- coding: UTF-8 -*-

import random

################################################
# Classe Modele
################################################

class Dalek():
    def __init__(self,x,y):
        self.x=x 
        self.y=y

class Jeu():
    def __init__(self,parent):
        self.parent=parent
        self.dimx=8
        self.dimy=6
        self.nbDalekParNiveau = 5
        self.daleks = []
        self.niveau = 1
        self.points = 0
        self.creerNouveauNiveau()

    def prochaincoup(self,coup):
        self.doc.bouge(coup)
        for i in self.daleks:
            i.bouge(self.doc.x,self.doc.y)
            
    def creerNouveauNiveau(self):
        nbDaleks = self.nbDalekParNiveau * self.niveau
        pos=[]
        while nbDaleks:
            x=random.randrange(self.dimx)
            y=random.randrange(self.dimy)
            if [x,y] not in pos:
                pos.append([x,y])
                nbDaleks-=1
        for i in pos:
            self.daleks.append(Dalek(i[0],i[1]))
            
            
            
